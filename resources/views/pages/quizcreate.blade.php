@extends('layouts.master')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <br>
                <h2>เพิ่มการทดสอบ</h2>
                
                <br />
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                            </ul>
                    </div>
               @endif

               @if(\Session::has('success'))
               <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
               </div>
               @endif

                <form method="post" action="{{url('quizteststore', ['id'=>$id])  }}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <input type="number" name="testno" class="form-control" placeholder="การทดสอบครั้งที่" />
                    </div>
                    <div class="form-group">
                        <input type="text" name="description" class="form-control" placeholder="รายละเอียด(ไม่บังคับ)" />
                    </div>
                    <div class="form-group">
                        <a href="{{ route('classdetails', ['id'=>$id]) }}"> <button type="button" class="btn btn-outline-danger mt-4 mb-4">ยกเลิก</button> </a>
                        <input type="submit" class="btn btn-primary" value="บันทึก" />
                    </div>

                </form>
            </div>

        </div>
    </div>

@endsection