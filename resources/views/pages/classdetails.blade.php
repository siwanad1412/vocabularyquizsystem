@extends('layouts.master')

@section('top')
    @can('addwork') 
    <i class="fas fa-plus  mr-4 cursor hf-w-coler" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
           <li><a class="dropdown-item" href={{ route('workcreate', ['cid'=>$classes->ClassID]) }}>เพิ่มงาน</a></li>
        </div>
    @endcan   
@endsection

@section('content')

<div class="container">
        <div class="card-header bg-main p-5">
            <h5 class="m-0 fonts">{{ $classes['ClassName']}}</h5>
            <p class="mb-0 mt-2 fonts-D">
                กลุ่ม {{ $classes['ClassGroup'] }}
            </p>
            <p class="mb-0 mt-2 fonts-D">
                {{ $classes['TeacherName'] }}
            </p>
            <div align="right">
                <form  method="get" class="addmember_form" action="{{ action('ClassesController@managemember',$classes['ClassID'])  }}">
                    {{csrf_field()}}

                    @can('managemember') 
                    <button type="submit" class="btn btn-warning fonts-B">จัดการสมาชิก</button>
                    @endcan
                </form>

                <form  method="get" class="edit_form" action="{{ action('ClassesController@edit',$classes['ClassID'])  }}">
                    {{csrf_field()}}

                    @can('editclass')
                    <button type="submit" class="fonts-B btn btn-primary ">แก้ไข</button>
                    @endcan

                </form>
                
                <form  method="get" class="delete_form" action="{{ action('ClassesController@destroy', $classes['ClassID'])  }}">
                    {{csrf_field()}}

                    @can('editclass')
                    <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="fonts-B btn btn-danger" onclick="return confirm('คุณต้องการลบคลาส {{ $classes['ClassName'] }} ใช่หรือไม่ ?')">ลบคลาส</button>
                    @endcan
                </form>
            </div>
            @can('managemember') 
            <h5 class="m-0 fonts">รหัสชั้นเรียน :  {{ $classes['code'] }}</h5>
            @endcan
        </div>

        @foreach ($classquiz as $row)
            <a href="{{ url('/check', ['id'=>$row->id, 'cid'=>$classes['ClassID']]) }}">
                <div class="card cursor mt-4">
                    <div class="card-body">
                    <h5 class="card-title fonts-b">การทดสอบครั้งที่ {{ $row->testno }} : {{ $row->description }}</h5>
                        <p class="card-text fonts-D date-limit">{{ $row->created_at }}</p>
                        <div align="right">

                            <form  method="get" class="showallscore_form" action="{{ action('QuizzesController@allscore', ['id'=>$row->id, 'cid'=>$classes['ClassID']])  }}">
                                {{csrf_field()}}
                                @can('editclass')
                                <button type="submit" class="btn btn-warning fonts-B">ดูคะแนนทั้งหมด</button>
                                @endcan
                            </form>
    
                            <form  method="get" class="edit_form" action="{{ action('QuizzesController@edit', ['id'=>$row->id])  }}">
                                {{csrf_field()}}
                                @can('editclass')
                                <button type="submit" class="btn btn-primary fonts-B">แก้ไข</button>
                                @endcan
                            </form>
    
                            <form  method="get" class="delete_form" action="{{ action('QuizzesController@destroy',['id'=>$row->id])  }}">
                                {{csrf_field()}}
                                @can('editclass')
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class="btn btn-danger fonts-B" onclick="return confirm('คุณต้องการลบการทดสอบครั้งที่ {{ $row->testno }}  ใช่หรือไม่ ?')">ลบ</button>
                                @endcan
                            </form>
                        </div>
                    </div>
                </div>
            </a>
        

        @endforeach
        
        @foreach($classWorks as $row)
        <a href="{{ route('classquiz', ['id'=>$row->WorkID, 'cid'=>$row->ClassID]) }}">
            <div class="card cursor mt-4">
                <div class="card-body">
                    <h5 class="card-title fonts-b">สัปดาห์ที่ {{ $row->WorkWeek}} : {{ $row->WorkDescription }}</h5>
                    <p class="card-text fonts-b">ประเภท : {{ $row->WorkCategory }}</p>
                    <p class="card-text fonts-D date-limit">{{ $row->created_at }}</p>
                    <div align="right">

                        <form  method="get" class="edit_form" action="{{ action('WorkController@edit',['id'=>$row->WorkID])  }}">
                            {{csrf_field()}}
                            @can('editclass')
                            <button type="submit" class="fonts-B btn btn-primary">แก้ไข</button>
                            @endcan
                        </form>

                        <form  method="get" class="delete_form" action="{{ action('WorkController@destroy',['id'=>$row->WorkID])  }}">
                            {{csrf_field()}}
                            @can('editclass')
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="fonts-B btn btn-danger" onclick="return confirm('คุณต้องการลบงานสัปดาห์ที่  {{ $row->WorkWeek}} ใช่หรือไม่ ?')">ลบ</button>
                            @endcan
                        </form>
                    </div>
                </div>
            </div>
        </a>
        @endforeach
</div>

@endsection