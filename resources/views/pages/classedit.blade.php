@extends('layouts.master')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <br>
                <h4>สร้างชั้นเรียน</h4>
                <br />
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                            </ul>
                    </div>
               @endif

               @if(\Session::has('success'))
               <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
               </div>
               @endif

                <form method="post" action="{{ route('classupdate', $class['ClassID']) }}">
                    {{csrf_field()}}
                    <div class="form-group">
                        
                        <input type="text" name="ClassName" class="form-control" placeholder="" value="{{$class['ClassName']}}" />
                    </div>
                    <div class="form-group">
                        <input type="text" name="ClassGroup" class="form-control" placeholder="กลุ่ม" value="{{$class['ClassGroup']}}"/>
                    </div>
                    <div class="form-group">
                        <input type="text" name="ClassDetails" class="form-control" placeholder="รายละเอียด" value="{{$class['ClassDetails']}}"/>
                    </div>                    
                    <div class="form-group">
                        <a href="{{ URL::previous() }}"> <button type="button" class="btn btn-outline-danger mt-4 mb-4">ยกเลิก</button> </a>
                        <input type="submit" class="btn btn-success" value="อัพเดต" />
                    </div>

                </form>
            </div>

        </div>
    </div>

@endsection