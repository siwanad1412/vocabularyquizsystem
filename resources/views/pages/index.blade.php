@extends('layouts.master')


@section('top')
                @can('admin')
                    <div class="d-flex j align-items-center">
                        <a href="/manageusers"><button class="btn btn-primary">จัดการผู้ใช่</button></a>
                    </div>
                @endcan
        <i class="fas fa-plus  mr-4 cursor hf-w-coler" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <li><a class="dropdown-item" href="#" data-toggle="modal" data-target="#yourModal">เข้าร่วมชั้นเรียน</a></li>
            @can('addclass')
                <li><a class="dropdown-item" href="classcreate">สร้างชั้นเรียน</a></li>
            @endcan
            
        </div>    
@endsection

@section('content')
@if(\Session::has('Unsuccessful'))
<div class="alert alert-danger">
    <p>{{ \Session::get('Unsuccessful') }}</p>
</div>
@endif
@if(\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div>
    @endif 
<div class="modal fade" id="yourModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>        
    @endif
    @if(\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div>
    @endif
    @if(\Session::has('createsuccess'))
        <div class="alert alert-success">
            <p>{{ \Session::get('createsuccess') }}</p>
        </div>
    @endif
    
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <form method="post" action="{{ url('joinclass') }}">
        {{csrf_field()}}
            <div class="modal-body">
                <h2 class="m-0 fonts-b">เข้าร่วมชั้นเรียน</h2>
                <h5 class="m-0 fonts">ขอรหัสชั้นเรียนจากครู แล้วป้อนรหัสที่นี่</h5>
                <input type="text" name="code" id="code" placeholder="รหัสของชั้นเรียน"> </>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
            {{-- <a href="joinclass"><button class="btn btn-primary">เข้าร่วมชั้นเรียน</button></a> --}}
            <input type="submit" class="btn btn-primary" value="เข้าร่วมชั้นเรียน" />
            </div>
        </form>
      </div>
    </div>
  </div>

<div class="container">
    <div class="row">
        @foreach($classes as $row)
        <div class="col-md-4 res-card ex1">
                <a href="{{ route('classdetails', ['id'=>$row->ClassID]) }}">
                    <div class="card cursor">
                        <div class="card-header bg-main p-5">
                            <h5 class="m-0 fonts">{{ $row->ClassName }}</h5>
                            <p class="mb-0 mt-2 fonts-D">กลุ่ม {{ $row->ClassGroup }}</p>
                            <p class="mb-0 mt-2 fonts-D">{{ $row->TeacherName }}</p>
                        </div>
                        <div class="card-body hf-b-coler ex2">
                            <h5 class="card-title">{{ $row->ClassName }}</h5>
                            <p class="card-text text-limit">{{ $row->ClassDetails }}</p>
                        </div>
                    </div>
                </a>
        </div>
        @endforeach
    </div>
</div>

@endsection