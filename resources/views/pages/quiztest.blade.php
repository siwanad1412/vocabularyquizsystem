@extends('layouts.master')

@section('content')


    @if(count($errors) > 0)
        <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
        </div>
    @endif

    @if(\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div>
    @endif
<div class="resume-section-content">
    <h2 class="m-0 fonts-B">การทดสอบ</h2>
</div>

{{-- <div class="row">
    <div class="col-md-6">
        <div class="d-flex mt-3 fonts-B">
            คำศัพท์
        </div>
    </div>
    <div class="col-md-6">
        <div class="d-flex mt-3 fonts-B">
            ตัวเลือก 1
        </div>
    </div>
</div>

@foreach ($vocab as $row)
<div class="row">
    <div class="col-md-6">
        <div class="d-flex mt-3">
            <h2>{{ $row->Vocab  }}</h2>
        </div>
    </div>
    <div class="col-md-6">
        <div class="d-flex mt-3">
            <h2>{{ $row->Choice_1  }}</h2>
        </div>
    </div>
</div>
@endforeach --}}

<hr>

<form method="post" action="{{ url('checkscore', ['id'=>$quizid,'cid'=>$classid]) }}">
    {{csrf_field()}}
    @foreach ($vocab as $row)
    <div>
        <p>
            <span class="fonts-B">คำศัพท์</span> 
            <h3 class="fonts-B">{{ $row->Vocab  }}</h3>
        </p>

        <div class="row">
            
            <div class="col-md-6">
                <span class="fonts-B">ตัวเลือก</span>
                <div class="d-flex mt-3">
                    <div class="custom-control  mr-4">
                        <label class="" for="Choice_1">
                            <h1 class="fonts-B"><input type="checkbox" name="answer[]" value="{{ $row->Choice_1  }}"> <label>{{ $row->Choice_1  }}</label></h1>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="d-flex mt-3">
                    <div class="custom-control custom-checkbox mr-4">
                        <input type="checkbox" class="custom-control-input" id="Choice_2"
                            name="Choice_2">
                        <label class="" for="Choice_2">
                            <h1 class="fonts-B"><input type="checkbox" name="answer[]" value="{{ $row->Choice_2  }}"> <label>{{ $row->Choice_2  }}</label></h1>
                        </label>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="d-flex mt-3">
                    <div class="custom-control custom-checkbox mr-4">
                        <input type="checkbox" class="custom-control-input" id="Choice_3"
                            name="Choice_3">
                        <label class="" for="Choice_3">
                            <h1 class="fonts-B"><input type="checkbox" name="answer[]" value="{{ $row->Choice_3  }}"> <label>{{ $row->Choice_3  }}</label></h1>
                        </label>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="d-flex mt-3">
                    <div class="custom-control custom-checkbox mr-4">
                        <input type="checkbox" class="custom-control-input" id="Choice_4"
                            name="Choice_4">
                        <label class="" for="Choice_4">
                            <h1 class="fonts-B"><input type="checkbox" name="answer[]" value="{{ $row->Choice_4  }}"> <label>{{ $row->Choice_4  }}</label></h1>
                        </label>
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-6">
                    <span class="fonts-b">คำตอบ : </span> 
                    <input type="text" name="answer[]" id="answer[]" placeholder="ระบุคำตอบที่นี่"> </>            
            </div> --}}
        </div>
    </div>
    @endforeach
    <hr>
    <div align="left">
        <a href="/checkscore"><button class="btn btn-success">ส่งคำตอบ</button></a>
    </div>
</form>
    <hr>

    
@endsection