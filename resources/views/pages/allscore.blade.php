@extends('layouts.master')

@section('content')

    <div classs="resume-section-content">
        <p></p>
        {{-- <i class="fa" aria-hidden="true"><h2 class="mb-5 fa-address-book"> สรุปคะแนน</h2></i> --}}
        <div class="resume-section-content">
            <i class="" aria-hidden="true"><h2 class="mb-5 fas fa-check">สรุปคะแนน</h2></i>
        </div>
        <table class="table">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">ชื่อ</th>
                <th scope="col">คะแนน</th>
                {{-- <th scope="col">จัดการ</th> --}}
              </tr>
            </thead>
            <tbody>
                @foreach($score as $row)
                {{-- <form  method="get" class="addmember_form" action="{{ action('ClassesController@deletemember', ['uid'=>$row->userid])  }}"> --}}
                {{-- {{csrf_field()}} --}}
                    <tr>
                        <th scope="row">{{$row->userid}}</th>
                        <td>{{$row->name}}</td>
                        <td>{{$row->score}}</td>
                        {{-- <td>
                            <button type="submit" class="btn btn-danger" onclick="return confirm('คุณต้องการลบผู้ใช้ {{ $row->name }} ออกจากคลาสใช่หรือไม่ ?')">ลบ</button>
                        </td> --}}
                    </tr>
                {{-- </form> --}}
                @endforeach
            </tbody>
        </table>
        <a href="{{ url('/classdetails', ['id'=>$classid]) }}"><button type="submit" class="btn btn-success">ตกลง</button></a>
    </div>


@endsection