@extends('layouts.master')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <br>
                <h4>งาน</h4>
                <br />
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                            </ul>
                    </div>
               @endif

               @if(\Session::has('success'))
               <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
               </div>
               @endif

                <form method="post" action="{{ route('workupdate', $works['WorkID']) }}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <input type="number" name="WorkWeek" class="form-control" value="{{$works['WorkWeek']}}" placeholder="งานสัปดาห์ที่" />
                    </div>
                    <div class="form-group">
                        <input type="text" name="WorkDescription" class="form-control" value="{{$works['WorkDescription']}}" placeholder="รายละเอียดงาน(ไม่บังคับ)" />
                    </div>
                    <div class="form-group">
                        <input type="text" name="WorkCategory" id="WorkCategory" class="form-control" value="{{$works['WorkCategory']}}" />
                    </div>
                    <div class="form-group">
                        <a href="{{ URL::previous() }}"> <button type="button" class="btn btn-outline-danger mt-4 mb-4">ยกเลิก</button> </a>
                        <input type="submit" class="btn btn-primary" value="บันทึก" />
                    </div>

                </form>
            </div>

        </div>
    </div>

@endsection