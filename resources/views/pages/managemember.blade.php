@extends('layouts.master')

@section('content')

<div class="container">
    
    <div class="card-header bg-main p-5">
        <h5 class="m-0 fonts">{{ $classes['ClassName']}}</h5>
        <p class="mb-0 mt-2 fonts-D">
            กลุ่ม {{ $classes['ClassGroup'] }}
        </p>
        <p class="mb-0 mt-2 fonts-D">
            {{ $classes['TeacherName'] }}
        </p>
        <p></p>
        <h5 class="m-0 fonts">รหัสชั้นเรียน :  {{ $classes['code'] }}</h5>
    </div>

    <div classs="resume-section-content">
        <p></p>
        <i class="fa" aria-hidden="true"><h2 class="mb-5 fa-address-book"> จัดการสมาชิก</h2></i>
        <table class="table">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">ชื่อ</th>
                <th scope="col">สถานะ</th>
                <th scope="col">จัดการ</th>
              </tr>
            </thead>
            <tbody>
            @foreach($classdetails as $row)
            <form  method="get" class="addmember_form" action="{{ action('ClassesController@deletemember',['id'=>$classes['ClassID'], 'uid'=>$row->userid])  }}">
              {{csrf_field()}}
                <tr>
                    <th scope="row">{{$row->userid}}</th>
                    <td>{{$row->name}}</td>
                    <td>{{$row->status}}</td>
                    <td>
                        <button type="submit" class="btn btn-danger" onclick="return confirm('คุณต้องการลบผู้ใช้ {{ $row->name }} ออกจากคลาสใช่หรือไม่ ?')">ลบ</button>
                    </td>
                </tr>
            </form>
              @endforeach
            </tbody>
          </table>
          

          {{-- <div class="resume-section-content">
          <h2 class="mb-5">สมาชิกที่ไม่ได้เข้าร่วม</h2>
          </div> --}}

          {{-- <table class="table">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">ชื่อ</th>
                <th scope="col">สถานะ</th>
                <th scope="col">จัดการ</th>
              </tr>
            </thead>
            <tbody>
            @foreach($users as $row)
            <form  method="get" class="addmember_form" action="{{ action('ClassesController@addmember',['id'=>$classes['ClassID'], 'uid'=>$row->userid])  }}">
              {{csrf_field()}}
              <tr>
                <th scope="row">{{$row->userid}}</th>
                <td>{{$row->name}}</td>
                <td>{{$row->status}}</td>
                <td>
                    <input type="submit" class="btn btn-primary" value="เพิ่มสมาชิก" />
                </td>
             </tr>
            </form>
            @endforeach
          
            </tbody>
          </table> --}}
    </div>

</div>

@endsection