@extends('layouts.master')

@section('content')

<div class="container">

    <div classs="resume-section-content">
        <p></p>
        <h2 class="mb-5">จัดการผู้ใช้ 
          {{-- <button type="submit" class="btn btn-primary">Add Member</button></h2> --}}
        <table class="table">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">ชื่อ</th>
                <th scope="col">สถานะ</th>
                <th scope="col">การจัดการ</th>
              </tr>
            </thead>
            <tbody>
            @foreach($users as $row)
                <tr>
                    <th scope="row">{{$row->userid}}</th>
                    <td>{{$row->name}}</td>
                    <td>{{$row->status}}</td>
                    <td>
                        <form  method="get" class="edit_form" action="{{ action('ManageusersController@edit',$row['userid'])  }}">
                            {{csrf_field()}}
                            @can('manageuser')
                            <button type="submit" class="fonts-B btn btn-primary ">แก้ไข</button>
                            @endcan
                        </form>
                        <form  method="get" class="delete_form" action="{{ action('ManageusersController@destroy',$row['userid'])  }}">
                          {{csrf_field()}}
                          @can('manageuser')
                          <button type="submit" class="fonts-B btn btn-danger" onclick="return confirm('คุณต้องการลบผู้ใช้ {{ $row['userid'] }} ใช่หรือไม่ ?')">ลบ</button>
                          @endcan
                      </form>
                    </td>
                </tr>
              @endforeach
            </tbody>
          </table>
    </div>

</div>

@endsection