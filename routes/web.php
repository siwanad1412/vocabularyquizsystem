<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', 'HomeController@index');

// Route::get('quiz/{id}', 'HomeController@quiz');

Route::get('/', 'PagesController@index')->middleware('auth');
//Route::get('/detail', 'PagesController@detail');
//Route::get('/quiz', 'PagesController@quiz');

Route::get('/classcreate', 'ClassesController@create')->middleware('auth')->middleware('admin');
Route::post('/classstore', 'ClassesController@store')->middleware('auth')->middleware('admin');
Route::get('/classedit/{id}', 'ClassesController@edit')->name('classedit')->middleware('auth')->middleware('admin');
Route::get('/classdelete/{id}', 'ClassesController@destroy')->middleware('auth')->middleware('admin');
Route::post('/classupdate/{id}', 'ClassesController@update')->name('classupdate')->middleware('auth')->middleware('admin');

Route::post('/joinclass', 'ClassesController@joinclass')->name('joinclass')->middleware('auth');


Route::get('/managemember/{id}', 'ClassesController@managemember')->middleware('auth')->middleware('admin');
Route::get('/addmember/{id}/{uid}', 'ClassesController@addmember')->name('addmember');
Route::get('/deletemember/{id}/{uid}', 'ClassesController@deletemember')->name('deletemember')->middleware('admin');
Route::get('/search', 'ClassesController@search');

Route::get('/classdetails/{id}', 'ClassesController@index')->name('classdetails')->middleware('auth');

Route::get('/quiz/{id}/{cid}', 'QuizzesController@index')->name('classquiz')->middleware('auth');
Route::post('/quizstore/{wid}', 'QuizzesController@store')->middleware('auth');
Route::get('/quizcreate/{id}', 'QuizzesController@quizcreate')->name('quizcreate')->middleware('auth')->middleware('admin');
Route::get('/quizedit/{id}', 'QuizzesController@edit')->middleware('auth')->middleware('admin');
Route::get('/quiztest/{id}/{cid}', 'QuizzesController@quiztest')->name('quiztest')->middleware('auth');
Route::post('/quizteststore/{id}', 'QuizzesController@quizteststore')->middleware('auth');
Route::post('/quiztestupdate/{id}', 'QuizzesController@update')->middleware('auth')->middleware('admin');
Route::get('/deletquiz/{id}', 'QuizzesController@destroy')->name('deletequiz')->middleware('admin');
Route::get('/showscore/{id}/{cid}', 'QuizzesController@showscore')->name('showscore')->middleware('auth');
Route::get('/check/{id}/{cid}', 'QuizzesController@check')->name('check')->middleware('auth');
Route::post('/checkscore/{id}/{cid}', 'QuizzesController@checkscore')->name('checkscore')->middleware('auth');
Route::get('/savescore/{score}/{id}', 'QuizzesController@savescore')->name('savescore')->middleware('auth');

Route::get('/allscore/{id}/{cid}', 'QuizzesController@allscore')->name('allscore')->middleware('auth');

Route::get('/login', 'PagesController@login');

Route::get('/workcreate/{cid}', 'WorkController@create')->name('workcreate')->middleware('auth')->middleware('admin');
Route::post('/workstore/{cid}', 'WorkController@store')->middleware('auth')->middleware('admin');
Route::get('/workdelete/{id}', 'WorkController@destroy')->middleware('auth')->middleware('admin');
Route::get('/workedit/{id}', 'WorkController@edit')->middleware('auth')->middleware('admin');
Route::post('/workupdate/{id}', 'WorkController@update')->name('workupdate')->middleware('auth')->middleware('admin');

Route::get('/manageusers', 'ManageusersController@index')->middleware('auth')->middleware('admin');
Route::get('/edituser/{id}', 'ManageusersController@edit')->name('edituser')->middleware('auth')->middleware('admin');
Route::get('/deleteuser/{id}', 'ManageusersController@destroy')->middleware('auth')->middleware('admin');
Route::post('/userupdate/{id}', 'ManageusersController@update')->name('userupdate')->middleware('auth')->middleware('admin');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


