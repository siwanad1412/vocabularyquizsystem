<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiztest extends Model
{
    protected $table = 'quiztest';
    protected $fillable = [
        'id',
        'classid',
        'testno',
        'userid',
        'description'
    ];
}
