<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public $timestamps = false;
    protected $table = 'Student';
    protected $fillable = [
        'StudentID',
        'StudentName',
    ];
}
