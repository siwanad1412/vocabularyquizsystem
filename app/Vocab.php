<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vocab extends Model
{
    public $timestamps = false;
    protected $table = 'Vocabulary';
    protected $fillable = [
        'VocabularyID',
        'WorkID',
        'StudentID',
        'userid',
        'Vocab',
    ];
}
