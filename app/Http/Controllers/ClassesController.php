<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Classroom;
use App\Quiz;
use App\Work;
use App\Vocab;
use App\User;
use App\Classmember;
use App\Quiztest;
use App\Quizscore;
use DB;

class ClassesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$id)
    {
        $user = $request->user();
        $classes = Classroom::where('ClassID',$id)->first();
        $classWorks = DB::table('Class')
        ->join('Work', 'Work.ClassID', '=', 'Class.ClassID')
        ->select('Class.ClassID', 'Class.ClassName', 'Class.ClassGroup', 'Class.TeacherName', 'Work.WorkID', 'WorkWeek', 'WorkDescription', 'WorkCategory', 'Work.created_at')
        ->where('Class.ClassID', '=', $id)
        ->orderBy('Work.created_at', 'DESC')
        ->get();

        $classquiz = DB::table('quiztest')
        ->join('Class', 'Class.ClassID', '=', 'quiztest.classid')
        ->select('quiztest.id', 'quiztest.classid','quiztest.testno', 'quiztest.description', 'quiztest.created_at')
        ->where('quiztest.classid', '=', $id)
        ->orderBy('quiztest.created_at', 'DESC')
        ->get();

        $score = DB::table('quizscore')
        ->join('quiztest', 'quiztest.id', '=', 'quizscore.quiztestid')
        ->select('quizscore.id', 'quizscore.userid','quizscore.quiztestid')
        ->where('quiztest.classid', '=', $id)
        ->where('quizscore.userid', '=', $user->userid)
        ->orderBy('quiztest.created_at', 'DESC')
        ->get();
        
        // dd($object);
        
        
        return view('pages.classdetails', compact('classes'), compact('classWorks', 'classquiz', 'score'));
    }

    /*public function class($id)
    {
        $class = Classroom::Where('ClassID',$id)->first();
        return view('classdetails', ['ClassID' => $ClassID]);
    }
    */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.classcreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [ 
            'ClassName' => 'required',
            'ClassGroup',
            'ClassDetails',
            'code',
            'TeacherID',
            'TeacherName' 
        ]);
        $code = Str::random(7);
        // dd($code);
        $class = new Classroom([
            'ClassName' => $request->get('ClassName'),
            'ClassGroup' => $request->get('ClassGroup'),
            'ClassDetails' => $request->get('ClassDetails'),
            'code' => $code,
            'TeacherID' => auth()->user()->userid,
            'TeacherName' => auth()->user()->name
        ]);
        $class->save();

        $classmember = new Classmember([
            'classid' => $request->get('classid', $class->id),
            'userid' => auth()->user()->userid,
        ]);
        $classmember->save();
        
        


        return redirect('/')->with('createsuccess', 'สร้างชั้นเรียนเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $class = Classroom::where('ClassID',$id)->first();
        //dd($class);
        return view('pages.classedit',compact('class'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'ClassName' => 'required',
            'ClassGroup',
            'ClassDetails',
        ]);
        $ClassName = $request->input('ClassName');
        $ClassGroup = $request->input('ClassGroup');
        $ClassDetails = $request->input('ClassDetails');

        DB::update('update Class set ClassName = ?, ClassGroup = ?, ClassDetails = ? where ClassID = ?', [$ClassName, $ClassGroup, $ClassDetails, $id]);

        return redirect('/')->with('success', 'อัพเดตข้อมูลเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    

    public function destroy($id)
    {
        $item = Classroom::where('ClassID',$id);
        // dd($item);
        $item->delete();

        return redirect('/');
    }

    public function managemember($id)
    {
        $users = DB::table('users')
        ->join('class_has_member', 'class_has_member.userid', '!=', 'users.userid')
        ->select('users.name', 'users.userid','users.status')
        ->get();
        // dd($users);
        
        $classes = Classroom::where('ClassID',$id)->first();
        $classdetails = DB::table('class')
        ->join('class_has_member', 'class_has_member.classid', '=', 'class.classid')
        ->join('users', 'users.userid', '=', 'class_has_member.userid')
        ->select('Class.ClassID', 'Class.ClassName', 'Class.ClassGroup', 'Class.ClassDetails','Class.TeacherName','users.userid', 'users.name', 'users.status')
        ->where('class_has_member.ClassID', '=', $id)
        ->get();

        return view('pages.managemember', compact('classdetails', 'classes', 'users'));
    }

    public function addmember($id,$uid)
    {
        $user = auth()->user();
        $classmember = new Classmember([
            'classid' => $id,
            'userid'  => $uid,
        ]);
        $classmember->save();
        return redirect()->back();
    }

    public function deletemember($id,$uid)
    {
        $item = Classmember::whereuserid($uid)->whereclassid($id);
        // dd($item);
        $item->delete();
        
        return redirect()->back();
    }

    public function joinclass(Request $request)
    {
        $request->validate([ 
            'code' => 'required',
        ]);

        $user = auth()->user();
        $classes = Classroom::all();
        $code = $request->input('code');
        

        for($i = 0;$i < count($classes) ;$i++){
            if($classes[$i]->code == $code){
                $classmember = new Classmember([
                    'classid' => $classes[$i]->ClassID,
                    'userid'  => $user->userid,
                ]);
                $classmember->save();
                return redirect()->back()->with('success','เข้าร่วมชั้นเรียนเรียบร้อย');
            }
        }
        return redirect()->back()->with('Unsuccessful','รหัสเข้าร่วมชั้นเรียนไม่ถูกต้อง');
    }

    // public function search(Request $request){
    //     $search =$request->get('search');
    //     $users = DB::table('users')->where('userid', 'like', '%' .$search. '%')->paginate(5);
    //     return redirect(['users' => $users])->back();

    // }


}
