<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classroom;
use App\User;
use App\Student;
use App\Teacher;
use DB;

class PagesController extends Controller
{
    public function index(){
        $user = auth()->user();
        //$classes = Classroom::all()->toArray();
        //dd($classes);

        $classes = DB::table('Class')
        ->join('class_has_member', 'class_has_member.classid', '=', 'Class.ClassID')
        ->select('Class.ClassID', 'Class.ClassName', 'Class.ClassGroup', 'ClassDetails', 'Class.TeacherName')
        ->where('class_has_member.userid', '=', $user->userid)
        ->get()->toArray();
        //dd($classes);
       
        if($user->status == 'admin'){
            auth()->user()->assignRole('admin');
            $user->syncPermissions(['admin','manageuser','managemember','addclass','joinclass','addwork','addvocab','editclass']);
        }else if($user->status == 'member'){
            auth()->user()->assignRole('member');
            $user->syncPermissions(['joinclass','addvocab']);
        }
        else if($user->status == 'teacher'){
            auth()->user()->assignRole('teacher');
            $user->syncPermissions(['managemember','addclass','joinclass','addwork','addvocab','editclass']);
        }
        
        return view('pages.index', compact('classes'));
    }
    public function detail(){
        return view('pages.detail');
    }
    public function quiz(){
        return view('pages.quiz');
    }
    public function classcreate(){
        return view('pages.classcreate');
    }
    public function login(){
        return view('pages.login');
    }
}
