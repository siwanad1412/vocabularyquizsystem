<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classroom;
use App\Quiz;
use App\Work;
use App\Vocab;
use App\Quiztest;
use App\Quizscore;
use DB;

class QuizzesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id,$cid)
    {
        $work = Work::where('WorkID',$id)->first();
        $quiz = DB::table('Work')
        ->join('Vocabulary', 'Vocabulary.WorkID', '=', 'Work.WorkID')
        ->join('Choice', 'Choice.VocabularyID', '=', 'Vocabulary.VocabularyID')
        ->select('WorkWeek', 'WorkDescription','Vocab','Answer')
        ->where('Work.WorkID', '=', $id)
        ->get();
        return view('pages.quiz', compact ('work'), compact('quiz'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $wid)
    {
        
        $this->validate($request, [
            'WorkID',
            'StudentID',
            'VocabularyID',
            'Vocab' => 'required',
            'Choice_1' => 'required',
            'Choice_2' => 'required',
            'Choice_3' => 'required',
            'Choice_4' => 'required',
            'Answer' => 'required'
        ]);

        DB::beginTransaction();

        $WorkID = $request->input('WorkID');
        $StudentID;
        $userid = auth()->user()->userid;
        $VocabularyID = $request->input('VocabularyID');
        $Vocab = $request->input('Vocab');
        $Choice_1 = $request->input('Choice_1');
        $Choice_2 = $request->input('Choice_2');
        $Choice_3 = $request->input('Choice_3');
        $Choice_4 = $request->input('Choice_4');
        $Answer = $request->input('Answer');

        

        $vocab = new Vocab([
            'VocabularyID' => $request->get('VocabularyID'),
            'WorkID' => $request->get('WorkID', $wid),
            'StudentID',
            'userid'  => auth()->user()->userid,
            'Vocab' => $request->get('Vocab'),
            
        ]);
        $vocab->save();
        //dd($vocab->id);

        $quiz = new Quiz([
            'VocabularyID' => $request->get('VocabularyID', $vocab->id),
            'Choice_1' => $request->get('Choice_1'),
            'Choice_2' => $request->get('Choice_2'),
            'Choice_3' => $request->get('Choice_3'),
            'Choice_4' => $request->get('Choice_4'),
            'Answer' => $request->get('Answer')
        ]);

        $quiz->save();

        DB::commit();
        //dd($request);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quiz = Quiztest::where('id',$id)->first();
        return view('pages.quizedit', compact('quiz'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'testno' => 'required',
            'description',
        ]);
        $testno = $request->input('testno');
        $description = $request->input('description');

        DB::update('update Quiztest set testno = ?, description = ? where id = ?', [$testno, $description, $id]);

        return redirect()->back()->with('success', 'อัพเดตข้อมูลเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Quiztest::where('id',$id);
        $item->delete();

        return redirect()->back();
    }
    public function quizcreate($id)
    {
        $vocab = DB::table('Vocabulary')
        ->join('choice', 'choice.VocabularyID', '=', 'Vocabulary.VocabularyID')
        ->select('Vocabulary.Vocab', 'choice.Choice_1', 'choice.Choice_2', 'choice.Choice_3', 'choice.Choice_4', 'Answer')
        ->get();
        // dd($vocab);
        return view('pages.quizcreate', compact('id'));
    }

    public function quiztest(Request $request, $id, $cid)
    {
        // $test = DB::table('Class')
        // ->join('Work', 'Class.ClassID', '=', 'Work.ClassID')
        // ->join('Vocabulary', 'Vocabulary.WorkID', '=', 'Work.WorkID')
        // ->join('Choice', 'Choice.VocabularyID', '=', 'Vocabulary.VocabularyID')
        // ->join('Quiztest', 'Quiztest.ClassID', '=', 'Class.ClassID')
        // ->select('Vocabulary.VocabularyID','Vocabulary.Vocab', 'choice.Choice_1',
        //          'choice.Choice_2', 'choice.Choice_3', 'choice.Choice_4', 'Answer','Quiztest.id')
        // ->where('Work.ClassID', '=', $cid)
        // ->get();
        $vocab = DB::table('Vocabulary')
        ->join('Choice', 'Choice.VocabularyID', '=', 'Vocabulary.VocabularyID')
        ->join('Work', 'Work.WorkID', '=', 'Vocabulary.WorkID')
        ->select('Vocabulary.VocabularyID','Vocabulary.Vocab', 'choice.Choice_1',
                 'choice.Choice_2', 'choice.Choice_3', 'choice.Choice_4', 'Answer')
        ->where('Work.ClassID', '=', $cid)
        ->get();

        $work = Quiztest::Where('classid', '=', $id)->get();
        $classid = $cid;
        $quizid = $id;
        
        // dd($test);
        return view('pages.quiztest', compact('vocab'), compact('classid','work','quizid') );
    }

    public function quizteststore(Request $request, $id)
    {
        
        $this->validate($request, [
            'testno' => 'required',
            'description',
        ]);

        DB::beginTransaction();

        $testno = $request->input('testno');
        $description = $request->input('description');
        $classid = $id;
        $userid = auth()->user()->userid;
        $quiz = new Quiztest([
            'testno' => $request->get('testno'),
            'description' => $request->get('description'),
            'classid' => $id,
            'userid'  => auth()->user()->userid,
        ]);
        $quiz->save();
        DB::commit();
        return redirect('/');
    }

    public function checkscore(Request $request, $id, $cid)
    {
        $user = $request->user();
        $request->validate([ 
            'answer.*' => 'required',
        ]);
        $vocab = DB::table('Vocabulary')
        ->join('Choice', 'Choice.VocabularyID', '=', 'Vocabulary.VocabularyID')
        ->join('Work', 'Work.WorkID', '=', 'Vocabulary.WorkID')
        ->select('Vocabulary.VocabularyID','Vocabulary.Vocab', 'choice.Choice_1',
                 'choice.Choice_2', 'choice.Choice_3', 'choice.Choice_4', 'Answer')
        ->where('Work.ClassID', '=', $cid)
        ->get();
        
        $score = Quizscore::wherequiztestid($id)->whereuserid($user)->first();
        $count = 0;
        $answer = $request->input(['answer']);
        // dd($vocab);
        for($i = 0;$i < count($answer) ;$i++){
            if($vocab[$i]->Answer == $answer[$i]){
                $score=$score+1;
            }
        }
        // dd($score);    
        for($i = 0;$i < count($answer) ;$i++){
                $count=$count+1;
        }    
        // dd($score);
        $scores = new Quizscore([
            'quiztestid' => $request->get('quiztestid', $id),
            'userid'  => auth()->user()->userid,
            'score' => $score,
        ]);
        $scores->save();
        // $score = Quizscore::wherequiztestid($id)->first();
        $score = DB::table('quizscore')
        ->join('quiztest', 'quiztest.id', '=', 'quizscore.quiztestid')
        ->select('quizscore.id', 'quizscore.userid','quizscore.quiztestid', 'quizscore.score')
        ->where('quizscore.quiztestid', '=', $id)
        ->where('quizscore.userid', '=', $user->userid)
        ->orderBy('quiztest.created_at', 'DESC')
        ->first();
        // dd($vocab);
        $classid = $cid;

        return view('pages.showscore', compact('user', 'vocab', 'score', 'answer', 'count', 'classid'));
    }

    public function showscore(Request $request, $id, $cid)
    {
        $user = $request->user();
        $request->validate([ 
            'answer.*' => 'required',
        ]);
        $vocab = DB::table('Class')
        ->join('Work', 'Class.ClassID', '=', 'Work.ClassID')
        ->join('Vocabulary', 'Vocabulary.WorkID', '=', 'Work.WorkID')
        ->join('Choice', 'Choice.VocabularyID', '=', 'Vocabulary.VocabularyID')
        ->join('Quiztest', 'Quiztest.ClassID', '=', 'Class.ClassID')
        ->select('Vocabulary.VocabularyID', 'Choice.Answer','Work.ClassID', 'Quiztest.id')
        ->where('Work.ClassID', '=', $id)
        ->get();
       
        $score = DB::table('quizscore')
        ->join('quiztest', 'quiztest.id', '=', 'quizscore.quiztestid')
        ->select('quizscore.id', 'quizscore.userid','quizscore.quiztestid', 'quizscore.score')
        ->where('quizscore.quiztestid', '=', $id)
        ->where('quizscore.userid', '=', $user->userid)
        ->orderBy('quiztest.created_at', 'DESC')
        ->first();
        // dd($score);
        // $score = Quizscore::select('score')->where('userid', $user['userid'])->first();
        // dd($score);
        $classid = $cid;

        return view('pages.showscore', compact('user', 'vocab', 'score', 'classid'));
    }

    // public function savescore(Request $request, $score, $id)
    // {
    //     $user = $request->user();
    //     $score1 = Quizscore::select('score')->where('userid', $user['userid'])->first();
    //     $scores = new Quizscore([
    //         'quiztestid' => $request->get('quiztestid', $id),
    //         'userid'  => auth()->user()->userid,
    //         'score' => $score,
    //     ]);
    //     // dd($scores);
    //     $scores->save();
    //     return redirect('/');
    // }

    public function check(Request $request, $id, $cid)
    {
        $user = $request->user();
        // $score = Quizscore::wherequiztestid($id)->whereuserid($user)->first();
        $score = DB::table('quizscore')
        ->join('quiztest', 'quiztest.id', '=', 'quizscore.quiztestid')
        ->select('quizscore.id', 'quizscore.userid','quizscore.quiztestid', 'quizscore.score')
        ->where('quizscore.quiztestid', '=', $id)
        ->where('quizscore.userid', '=', $user->userid)
        ->orderBy('quiztest.created_at', 'DESC')
        ->first();
        // dd($score1);
        if($score != null){
            return redirect()->route('showscore', [$id, $cid]);
        }else
            if($score == null){
            return redirect()->route('quiztest', [$id, $cid]);
        }
        // dd($score);
        
    }

    public function allscore(Request $request, $id, $cid)
    {
        $score = DB::table('quizscore')
        ->join('quiztest', 'quiztest.id', '=', 'quizscore.quiztestid')
        ->join('users', 'quizscore.userid', '=', 'users.userid')
        ->select('quizscore.id', 'quizscore.userid','users.name', 'quizscore.quiztestid', 'quizscore.score')
        ->where('quizscore.quiztestid', '=', $id)
        ->orderBy('quiztest.created_at', 'DESC')
        ->get();
        // dd($score);
        $classid = $cid;
        

        return view('pages.allscore', compact('score', 'classid'));
    }
}
