<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classroom;
use App\Quiz;
use App\Work;
use App\Vocab;
use DB;

class WorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($cid)
    {
        $work = Work::where('ClassID', $cid)->first();
        return view('pages.workcreate', compact ('work'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($cid)
    {
        
        return view('pages.workcreate', ['classid' => $cid]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $cid)
    {
        
        $this->validate($request, [
            'WorkWeek' => 'required',
            'WorkDescription',
            'WorkCategory' => 'required',
            'ClassID' => $cid,
        ]);

        $work = new Work([
            'WorkWeek' => $request->get('WorkWeek'),
            'WorkDescription' => $request->get('WorkDescription'),
            'WorkCategory' => $request->get('WorkCategory'),
            'ClassID' => $request->get('ClassID', $cid)
        ]);

        //dd($work);
        $work->save();
        
        return redirect()->back();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $works = Work::where('WorkID',$id)->first();
        return view('pages.workedit', compact('works'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([ 
            'WorkWeek' => 'required',
            'WorkDescription',
            'WorkCategory' => 'required',
        ]);
        $WorkWeek = $request->input('WorkWeek');
        $WorkDescription = $request->input('WorkDescription');
        $WorkCategory = $request->input('WorkCategory');
        // dd($id);

        DB::update('update Work set WorkWeek = ?, WorkDescription = ?, WorkCategory = ? where WorkID = ?', [$WorkWeek, $WorkDescription, $WorkCategory, $id]);

        return redirect()->back()->with('success', 'อัพเดตข้อมูลเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Work::where('WorkID',$id);
        $item->delete();

        return redirect()->back();
    }
}
