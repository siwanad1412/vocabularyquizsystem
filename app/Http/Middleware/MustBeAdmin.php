<?php

namespace App\Http\Middleware;

use Closure;

class MustBeAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if($user->status == 'admin'){
            return $next($request);
        }
        else if($user->status == 'teacher'){
            return $next($request);
        }
        abort(403);
        

        
    }
}
