<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quizscore extends Model
{
    protected $table = 'quizscore';
    protected $fillable = [
        'id',
        'quiztestid',
        'userid',
        'score'
    ];
}
