<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classmember extends Model
{
    public $timestamps = false;
    protected $table = 'class_has_member';
    protected $fillable = ['classid', 'userid'];
}
